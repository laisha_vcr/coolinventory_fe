import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './WebAdmin/login/login.component';
import { DashboardComponent } from './WebAdmin/dashboard/dashboard.component';
import { InicioComponent } from './WebAdmin/inicio/inicio.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InitiatedComponent } from './initiated/initiated.component';
import { LandingComponent } from './landing/landing.component';
import { EnteringComponent } from './entering/entering.component';
import { AgregarProductoComponent } from './initiated/agregar-producto/agregar-producto.component';
import { RegistrarseComponent } from './entering/registrarse/registrarse.component';
import { StockComponent } from './landing/stock/stock.component';
import { RouterModule } from '@angular/router';
import { PerfilComponent } from './initiated/perfil/perfil.component';
import { DatosCompraComponent } from './initiated/datos-compra/datos-compra.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    InicioComponent,
    NavbarComponent,
    InitiatedComponent,
    LandingComponent,
    EnteringComponent,
    AgregarProductoComponent,
    RegistrarseComponent,
    StockComponent,
    PerfilComponent,
    DatosCompraComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
