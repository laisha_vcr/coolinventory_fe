import { DatosCompraComponent } from './initiated/datos-compra/datos-compra.component';
import { PerfilComponent } from './initiated/perfil/perfil.component';
import { StockComponent } from './landing/stock/stock.component';
import { RegistrarseComponent } from './entering/registrarse/registrarse.component';
import { AgregarProductoComponent } from './initiated/agregar-producto/agregar-producto.component';
import { InitiatedComponent } from './initiated/initiated.component';
import { EnteringComponent } from './entering/entering.component';
import { LandingComponent } from './landing/landing.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './WebAdmin/dashboard/dashboard.component';
import { InicioComponent } from './WebAdmin/inicio/inicio.component';
import { LoginComponent } from './WebAdmin/login/login.component';

  const routes: Routes = [
  
    {
      path: 'landing' , component: LandingComponent,
      children: [
        {path: '' , component: InicioComponent, },
        {path: 'inicio', component: InicioComponent}
      ]
    },
  
    {
      path: 'entering' , component: EnteringComponent,
      children: [
        // {path: '', redirectTo:'entering/dashboard', pathMatch:'full'},
        {path: '', component: DashboardComponent,
          children: [
            
            {path:'login', component: LoginComponent },
            {path: 'registrarse', component: RegistrarseComponent},
            {path: 'stock', component: StockComponent},
            {path: 'agregar-producto', component: AgregarProductoComponent}
      ]},
    ]
    },
    
    {
      path: 'initiated' , component: InitiatedComponent,
      children: [
        {path: '' , component: InitiatedComponent, },
        {path: 'perfil', component: PerfilComponent},
        {path: 'datos-compra', component: DatosCompraComponent}
        
      ]
    },
    
    {path: '**', redirectTo:'/landing', pathMatch:'full'}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
